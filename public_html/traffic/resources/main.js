
var upKey, downKey, leftKey, rightKey;

var Road = function Road(layout) {
    
}

var Player = function Player() {
     
}

window.onload = function() {

    loadLevel();
    var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game', { preload: preload, create: create, update: update, render: render });
    var road = {
        lanes:      4,
        laneWidth:  100,
        pos: 0
    };

    function preload () {
        game.load.image('p_car', 'img/p_car.png');
        game.load.image('lane_divider', 'img/l_stripe.png');
    }

    function create () {
        upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

        var centerX = game.world.centerX;

        // "road"
        var a_divider = game.add.sprite(-100, -100, 'lane_divider');
        dividers = game.add.group();
        for (var i = 0; i < road.lanes; i++) {
            for (var j = 0; j < ( (game.world.height + 2*a_divider.height) / (a_divider.height + 2*a_divider.height) ); j++) {
                var divider = dividers.create(centerX + i * road.laneWidth, j * 3 * a_divider.height, 'lane_divider');
            }
        }

        dividers.update = function(p) {
            this.forEach(function(divider) {
                if (divider.y > game.world.height + divider.height) {
                    divider.y = 0 - 3*divider.height;
                } else {
                    divider.y += p.speed;
                }
            });
        }

        // get ready
        var startingPlayerX = (game.world.centerX + a_divider.width/2 - road.laneWidth/2 - (game.cache.getImage('p_car').width/2));
        var startingPlayerY = game.world.height - 40 - game.cache.getImage('p_car').height;

        // player car
        player = game.add.sprite(startingPlayerX, startingPlayerY, 'p_car');

        player.changeDur = 60;
        player.sinceChange = 0;
        player.canChange = true;
        player.laneShiftWidth = road.laneWidth;
        player.speed = 10;
        player.roadPos = 0;

        player.update = function () {
            this.roadPos += this.speed;
            if (this.canChange == false) {
                this.sinceChange++;
                if (this.sinceChange > this.changeDur) {
                    this.sinceChange = 0;
                    this.canChange = true;
                }
            }
        }

        player.changeLaneLeft = function() {
            if (this.canChange) {
                this.x -= this.laneShiftWidth;
                this.canChange = false;
            } 
        }

        player.changeLaneRight = function() {
            if (this.canChange) {
                this.x += this.laneShiftWidth;
                this.canChange = false;
            } 
        }
    }

    function update() {
        player.update();
        dividers.update(player);

        // player controls
        if (leftKey.isDown) {
            player.changeLaneLeft();
        } else if (rightKey.isDown) {
            player.changeLaneRight();
        }

        if (upKey.isDown) {
            player.speed += 0.5;
        } else if (downKey.isDown && player.speed > 0) {
            player.speed -= 0.75;
        } else if (player.speed <= 0){
            player.speed = 0;
        }
    }

};

