// Traffic "namespace", application object, etc
var Traffic = {}; 

// creating state
Traffic.Boot = function(game) {};
Traffic.Boot.prototype = {
	preload: function() {
		// loading of indicator image for preload
	},
	create: function() {
		console.log("booting");

		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.pageAlignHorizontally = true;
		this.scale.pageAlignVertically = true;
		this.scale.updateLayout( true );
		
		this.game.state.start("Title");
	},
};

