var gameOver = function(game) {
    console.log("you lose");
};

gameOver.prototype = {
    create: function() {

    },

    toMenu: function() {
        this.state.start("Title");
    },

    replay: function() {
        this.state.start("Play");
    }
}
