/***********************************************
 * Player
 ***********************************************/
Traffic.Player = function(game) {
    // ? not sure if this is what I should do, kinda feel it should be glob
    this.game = game;

    // display properties
    this.sprite = null;
    this.spriteImg = "car";
    this.startX = this.game.world.centerX;
    this.startY = this.game.world.height - 80;
    this.laneShiftWidth = null;

    // control alias
    this.upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    this.downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    this.leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    this.rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

    // magic feel numbers
    this.speed = null;
    this.decelRate = -0.006;
    this.brakeRate = -0.04;
    this.accelRate = 0.01;
    this.maxSpeed = 12;

    // this should probably be an animation the player "launches", not instant
    this.canChange = true;
    this.changeDur = 60;
    this.sinceChange = 0;

    // future functionality
    this.isCruising = false;
}

Traffic.Player.prototype = {
    create: function() {
        this.speed = Traffic.startSpeed;
        this.sprite = this.game.add.sprite(this.startX, this.startY, this.spriteImg);
        this.sprite.x -= this.sprite.width / 2;
        this.sprite.x -= 3; // magic
        this.laneShiftWidth = Traffic.laneWidth;
    },

    update: function() {
        // check for lane change
        if (this.canChange == true) {
            if (this.leftKey.isDown) {
                this.shiftLeft();
            } else if (this.rightKey.isDown) {
                this.shiftRight();
            }
        } else { // can't change lanes yet
            this.sinceChange++;
            if (this.sinceChange > this.changeDur) {
                this.sinceChange = 0;
                this.canChange = true;
            }
        }

        // check for speed change
        if (this.upKey.isDown && this.speed < this.maxSpeed) {
            // accelerating
            this.speed += this.accelRate;
        } else if (this.downKey.isDown) {
            // braking
            if (this.speed > 0) {
                this.speed += this.brakeRate;
            // stopped
            } else {
                this.speed = 0;
            }
        } else if (this.speed > 0 && this.isCruising == false) {
            // cruising -> slowing down
            this.speed += this.decelRate;
        }
    },

    shiftLeft: function() {
        this.sprite.x -= this.laneShiftWidth;
        this.canChange = false;
    },

    shiftRight: function() {
        this.sprite.x += this.laneShiftWidth;
        this.canChange = false;
    },

    getSpeed: function() {
        return this.speed;
    },
};

/***********************************************
 * Road
 ***********************************************/
Traffic.Road = function (game) {
    this.game = game;
    this.lineSprite = "stripe";
    this.sprites = null;

    this.laneWidth = null;
    this.lanesHeight = null; // the "conveyer belt"

    this.startSpeed = 0.0;
    this.lanes = 5;
    this.vertLanes = 8;		// minimum number so that they are displayed on screen
    this.laneSpacing = 6;	// determines space between dividers vertically

    this.lastPos = 0;
};

Traffic.Road.prototype = {
    create: function() {
        // example divider
        a_divider = this.game.add.sprite(-100, -100, this.lineSprite);

        this.laneWidth = (this.game.world.width / this.lanes);
        this.laneHeight = a_divider.height * this.vertLanes * this.laneSpacing;

        // adding dividers to create visual lanes
        this.sprites = this.game.add.group();
        for (var i = 0; i < this.lanes; i++) {
            for (var j = 0; j < this.vertLanes; j++) {
                this.sprites.create(i*this.laneWidth - a_divider.width, j*this.laneSpacing * a_divider.height, this.lineSprite);
            }
        }
    },

    update: function() {
        // update line positions:
        diff = Traffic.position - this.lastPos;

        // update lines
        worldHeight = this.game.world.height; // local
        laneHeight = this.laneHeight;
        this.sprites.forEach(function(divider) {
            if (divider.y + diff >= laneHeight) {
                divider.y = 0 - divider.height;
            } else {
                divider.y += diff;
            }
        });

        // set for next update
        this.lastPos = Traffic.position;
    },

}; // end Road

/************************
 * Traffic
 ************************/
 Traffic.Drivers = function() {};
 Traffic.Drivers.prototype = {
	//
 };
