// game.js -- 

// creating the "Game" state 
Traffic.Game = function(game) {
	this.game = game;
	this.player	= null;
	this.road = null;

	// eventually loaded by track layout
	Traffic.position = 0;
	Traffic.laneWidth = 0;
	Traffic.currentLevel = 0;
	Traffic.startSpeed = 0;
};

Traffic.Game.prototype = {
	init: function() {
		// set Traffic globals from level data
		Traffic.laneWidth = this.game.width / 5; 
	},
	create: function() {
		this.road = new Traffic.Road(this.game);
		this.road.create();

		this.player = new Traffic.Player(this.game);
		this.player.create();

	},
	update: function() {
		Traffic.position += this.player.getSpeed();
		this.player.update();
		this.road.update();
	},
};
