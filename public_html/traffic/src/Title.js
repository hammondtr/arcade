Traffic.Title = function(game) {};

Traffic.Title.prototype = {
    preload: function() {
        this.game.load.image("splashTitle", "img/title.png");
        this.game.load.image("start", "img/start.png");
        this.game.load.image("car", "img/p_car.png");
        this.game.load.image("stripe", "img/l_stripe.png");
    },

    create: function() {
        var gameTitle = this.game.add.sprite(160,160,"splashTitle");    
        gameTitle.anchor.setTo(0.5, 0.5);

        var startButton = this.game.add.button(160,320,"start",this.startGame,this);
        startButton.anchor.setTo(0.5,0.5);
    },

    startGame: function() {
        console.log("game started");
        this.game.state.start("Play");
    }
}
